import 'package:flutter/material.dart';
void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BelajarListView(),
    );
  }
}
class BelajarListView extends StatelessWidget {
  final List nama = [
    "Image 1",
    "Image 2",
    "Image 3",
    "Image 4",
    "Image 5",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("UTS"),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
              title: Text(
                nama[index],
                style: TextStyle(fontSize: 30),
              ),
              //subtitle: Text('Nama  : Flamboyant Dharmawan Saputra'),
              leading: CircleAvatar(
                child: Text(
                  nama[index][0],
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          );
        },
        itemCount: nama.length,
      ),
    );
  }
}